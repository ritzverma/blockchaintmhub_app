
const triggerServerRequest = (options = {}, callback) => {

    const url = options.url || '/';

    const method = options.method || 'GET';

    const data = options.data || {};

    var urlEncodedData = "";

    var urlEncodedDataPairs = [];

    for (let name in data) {

        if (data[name])

            urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));

    }

    urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');



    const request = new Promise((resolve, reject) => {

        const xhr = new XMLHttpRequest();

        xhr.open(method, url);

        xhr.addEventListener('load', () => {

            resolve({

                response: xhr.response,

                status: xhr.status

            })

        })

        xhr.addEventListener('error', () => {

            reject({

                response: xhr.response,

                status: xhr.status

            })

        })

        if (urlEncodedData) {

            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

            xhr.send(urlEncodedData);

        } else

            xhr.send();

    }).then(res => {

        callback(res.response, res.status);

    }).catch(res => {

        callback(res.response, res.status);

    })

}


const ColorServiceURL = {

    index: '/',

    createColor: '/color',

    colorById: '/color/item/:colorId',

    getColorList: '/color/list',

}



const ColorService = {

    index: (callback) => {

        triggerServerRequest({

            url: ColorServiceURL.index,

            method: 'GET'

        }, callback);

    },

    createColor: ({ name, description, iconClass }, callback) => {

        triggerServerRequest({

            url: ColorServiceURL.createColor,

            method: 'POST',

            data: { name, description, iconClass }

        }, callback);

    },

    getColorById: (colorId, callback) => {

        triggerServerRequest({

            url: ColorServiceURL.colorById.replace(':colorId', colorId),

            method: 'GET',

        }, callback);

    },

    getColorList: (callback) => {

        triggerServerRequest({

            url: ColorServiceURL.getColorList,

            method: 'GET',

        }, callback);

    },

    removeColors: (colorId, callback) => {

        triggerServerRequest({

            url: ColorServiceURL.colorById.replace(':colorId', colorId),

            method: 'DELETE',

        }, callback);

    }

}



ColorService.index((res) => {

    console.log(res);

})

ColorService.createColor({ name: 'bbcol', description: 'bbcoldes', iconClass: '' }, (res) => {

    console.log(res);

});

ColorService.getColorById('6', (res) => {

    console.log(res);

});

ColorService.getColorList((res) => {

    console.log(res);

});

ColorService.removeColors('5', (res) => {

    console.log(res);

})




