import { Component } from '@angular/core';
import { ApiWrapperService } from './api-wrapper.service';
import { Observable } from 'rxjs/Observable';
import {Color} from './color';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private apiService: ApiWrapperService){};
  
  currentColor : string;
  
  colors : Color[];
  
  ngOnInit(){
    this.apiService.currentColor.subscribe(currentColor => this.currentColor = currentColor);
    this.getColorList();
  }
  getColorList(): void {
    this.apiService.getColorList()
        .subscribe(colors => this.colors = colors);
  }
  
}
