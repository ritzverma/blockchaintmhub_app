import { Component, OnInit } from '@angular/core';
import { ApiWrapperService } from '../api-wrapper.service';
import { Observable } from 'rxjs/Observable';
import {Color} from '../color';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  constructor(private apiService: ApiWrapperService){};
  
  currentColor : string;
  
  colors : Color[];
  
  ngOnInit(){
    this.apiService.currentColor.subscribe(currentColor => this.currentColor = currentColor);
    this.getColorList();
  }
  getColorList(): void {
    this.apiService.getColorList()
        .subscribe(colors => this.colors = colors);
  }
}
