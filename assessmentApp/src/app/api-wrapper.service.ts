import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Color} from './color';
import 'rxjs/add/operator/map';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()
export class ApiWrapperService {
  
  constructor(private http: HttpClient) { }
  private apiUrl = 'http://localhost:3003';

  ColorServiceURL = {

    index: '/',

    createColor: '/color',

    colorById: '/color/item/:colorId',

    getColorList: '/color/list',
  }
  getColorList():Observable<Color[]>  {
    return this.http.get<Color[]>(`${this.apiUrl}${this.ColorServiceURL.getColorList}`, httpOptions)
  }
  
  createColor(name, description, iconClass): Observable<Color[]> {
    return this.http.post<Color[]>(`${this.apiUrl}${this.ColorServiceURL.createColor}`,{
      name: name,
      description: description,
      iconClass : iconClass
    }, httpOptions)
  }
  removeColor(colorId): Observable<Color[]>{
    return this.http.delete<Color[]>(`${this.apiUrl}${this.ColorServiceURL.colorById.replace(':colorId', colorId)}`, httpOptions)
  }
  private log(message: string) {
    console.log(message);
  }
  
  private categoryColor = new BehaviorSubject<string>("#fff");
  currentColor = this.categoryColor.asObservable();

  

  changeColor(currentColor: string) {
    this.categoryColor.next(currentColor)
  }
  
}
