import { Component, OnInit, Input } from '@angular/core';
import { ApiWrapperService } from '../api-wrapper.service';
import { Observable } from 'rxjs/Observable';
import {Color} from '../color';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private apiService: ApiWrapperService){};
  
  currentColor: string;
  
  @Input() colors : Color[];
  ngOnInit(){
    this.apiService.currentColor.subscribe(currentColor => this.currentColor = currentColor)
  }
  changeColor(colorName): void{
    this.apiService.changeColor(colorName);
  }
}
