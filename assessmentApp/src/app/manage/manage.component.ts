import { Component, OnInit } from '@angular/core';
import { ApiWrapperService } from '../api-wrapper.service';
import { Observable } from 'rxjs/Observable';
import {Color} from '../color';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManageComponent implements OnInit {

  constructor(private apiService: ApiWrapperService){};
  colors : Color[];
  ngOnInit(){
    this.getColorList();
  }
  getColorList(): void {
    this.apiService.getColorList()
        .subscribe(colors => this.colors = colors);
  }
  createColor(form : NgForm): void {
    debugger;
    const formValues = form.value;
    this.apiService.createColor(formValues.colorName, formValues.colorDescription, formValues.colorClass )
    .subscribe(colors => this.getColorList());
  }
  removeColor(colorId): void{
    this.apiService.removeColor(colorId)
    .subscribe(colors => this.getColorList());
  }

}
