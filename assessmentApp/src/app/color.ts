export class Color {
    id:number;
    name: string;
    description: string;
    iconClass : string;
}
