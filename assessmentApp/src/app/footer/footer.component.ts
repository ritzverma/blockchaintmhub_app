import { Component, OnInit } from '@angular/core';
import { ApiWrapperService } from '../api-wrapper.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(private apiService: ApiWrapperService){};
  
  currentColor : string;

  ngOnInit() {
    this.apiService.currentColor.subscribe(currentColor => this.currentColor = currentColor)
  }

}
