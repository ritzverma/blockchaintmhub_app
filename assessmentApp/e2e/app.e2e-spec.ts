import { AssessmentAppPage } from './app.po';

describe('assessment-app App', () => {
  let page: AssessmentAppPage;

  beforeEach(() => {
    page = new AssessmentAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
