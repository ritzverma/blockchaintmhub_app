var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var routes = require('./routes/color');
var port = 3003;

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', routes);

app.listen(port, function () {
    console.log('COLOR MGT Backend is running in port ' + port + '!');
});


